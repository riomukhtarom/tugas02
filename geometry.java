import java.util.*;

public class geometry{
	public static void MainMenu(){
		System.out.println("Silahkan Pilih Bangun yang Akan dihitung (1-3)");
		System.out.println("1. Bangun Datar");
		System.out.println("2. Bangun Ruang");
		System.out.println("3. Keluar");
		System.out.print("==>");
	}
	
	public static void SubMenuTwoDimensionalGeometry(){
		Scanner input = new Scanner(System.in);
		int repeat=1;
		do{
			System.out.println("Bangun Datar");
			System.out.println("1. Persegi");
			System.out.println("2. Persegi Panjang");
			System.out.println("3. Segitiga Sama Kaki");
			System.out.println("4. Segitiga Sama Sisi");
			System.out.println("5. Trapesium");
			System.out.println("6. Jajaran Genjang");
			System.out.println("7. Lingkaran");
			System.out.print("==>");
			
			try{
				int choice = input.nextInt();
				switch(choice){
					case 1:
						repeat=1;
						do{
							try{
								SubMenuAreaAndPerimeter();
								int choice1 = input.nextInt();
								switch(choice1){
									case 1:
											AreaofSquare();
											repeat=0;
											break;
									case 2:
											PerimeterofSquare();
											repeat=0;
											break;
									default:
											System.out.println("Pilihan Salah");
											break;
								}
							}catch(InputMismatchException e){
									System.out.println("Masukkan Bilangan Bulat Positif!!!");
									input.nextLine();
									repeat=1;
							}    
						}while(repeat==1);
						break;
					case 2:
						repeat=1;
						do{
							try{
								SubMenuAreaAndPerimeter();
								int choice2 = input.nextInt();
								switch(choice2){
									case 1:
											AreaofRectangle();
											repeat=0;
											break;
									case 2:
											PerimeterofRectangle();
											repeat=0;
											break;
									default:
											System.out.println("Pilihan Salah");
											break;
								}
							}catch(InputMismatchException e){
									System.out.println("Masukkan Bilangan Bulat Positif!!!");
									input.nextLine();
									repeat=1;
							}    
						}while(repeat==1);
						break;
					case 3:
						repeat=1;
						do{
							try{
								SubMenuAreaAndPerimeter();
								int choice3 = input.nextInt();
								switch(choice3){
									case 1:
											AreaofTriangle();
											repeat=0;
											break;
									case 2:
											PerimeterofTriangle();
											repeat=0;
											break;
									default:
											System.out.println("Pilihan Salah");
											break;
								}
							}catch(InputMismatchException e){
									System.out.println("Masukkan Bilangan Bulat Positif!!!");
									input.nextLine();
									repeat=1;
							}    
						}while(repeat==1);
						break;
					case 4:
						repeat=1;
						do{
							try{
								SubMenuAreaAndPerimeter();
								int choice4 = input.nextInt();
								switch(choice4){
									case 1:
											AreaofEquilateralTriangle();
											repeat=0;
											break;
									case 2:
											PerimeterofEquilateralTriangle();
											repeat=0;
											break;
									default:
											System.out.println("Pilihan Salah");
											break;
								}
							}catch(InputMismatchException e){
									System.out.println("Masukkan Bilangan Bulat Positif!!!");
									input.nextLine();
									repeat=1;
							}    
						}while(repeat==1);
						break;
					case 5:
						repeat=1;
						do{
							try{
								SubMenuAreaAndPerimeter();
								int choice5 = input.nextInt();
								switch(choice5){
									case 1:
											AreaofTrapezoid();
											repeat=0;
											break;
									case 2:
											PerimeterofTrapezoid();
											repeat=0;
											break;
									default:
											System.out.println("Pilihan Salah");
											break;
								}
							}catch(InputMismatchException e){
									System.out.println("Masukkan Bilangan Bulat Positif!!!");
									input.nextLine();
									repeat=1;
							}    
						}while(repeat==1);
						break;
					case 6:
						repeat=1;
						do{
							try{
								SubMenuAreaAndPerimeter();
								int choice6 = input.nextInt();
								switch(choice6){
									case 1:
											AreaofParallelogram();
											repeat=0;
											break;
									case 2:
											PerimeterofParallelogram();
											repeat=0;
											break;
									default:
											System.out.println("Pilihan Salah");
											break;
								}
							}catch(InputMismatchException e){
									System.out.println("Masukkan Bilangan Bulat Positif!!!");
									input.nextLine();
									repeat=1;
							}    
						}while(repeat==1);
						break;
					case 7:
						repeat=1;
						do{
							try{
								SubMenuAreaAndPerimeter();
								int choice7 = input.nextInt();
								switch(choice7){
									case 1:
											AreaofCircle();
											repeat=0;
											break;
									case 2:
											PerimeterofCircle();
											repeat=0;
											break;
									default:
											System.out.println("Pilihan Salah");
											break;
								}
							}catch(InputMismatchException e){
									System.out.println("Masukkan Bilangan Bulat Positif!!!");
									input.nextLine();
									repeat=1;
							}    
						}while(repeat==1);
						break;
					default:
							System.out.println("Pilihan Salah");
							break;
				}
				repeat=0;
			}catch(InputMismatchException e){
					System.out.println("Masukkan Bilangan Bulat Positif!!!");
					input.nextLine();
					repeat=1;
			}
			
		}while(repeat==1);
	}
	
	public static void SubMenuThreeDimensionalGeometry(){
		Scanner input = new Scanner(System.in);
		int repeat=1;
		do{
			System.out.println("Bangun Ruang");
			System.out.println("1. Kubus");
			System.out.println("2. Balok");
			System.out.println("3. Prisma Segitiga Samasisi");
			System.out.println("4. Limas Segitiga");
			System.out.println("5. Limas Segiempat");
			System.out.println("6. Tabung");
			System.out.println("7. Kerucut");
			System.out.println("8. Bola");
			System.out.print("==>");
			try{
				int choice = input.nextInt();
				switch(choice){
					case 1:
						repeat=1;
						do{
							try{
								SubMenuVolumeAndSurfaceArea();
								int choice1 = input.nextInt();
								switch(choice1){
									case 1:
											VolumeofCube();
											repeat=0;
											break;
									case 2:
											SurfaceAreaofCube();
											repeat=0;
											break;
									default:
											System.out.println("Pilihan Salah");
											break;
								}
							}catch(InputMismatchException e){
									System.out.println("Masukkan Bilangan Bulat Positif!!!");
									input.nextLine();
									repeat=1;
							}    
						}while(repeat==1);
						break;
					case 2:
						repeat=1;
						do{
							try{
								SubMenuVolumeAndSurfaceArea();
								int choice2 = input.nextInt();
								switch(choice2){
									case 1:
											VolumeofBeam();
											repeat=0;
											break;
									case 2:
											SurfaceAreaofBeam();
											repeat=0;
											break;
									default:
											System.out.println("Pilihan Salah");
											break;
								}
							}catch(InputMismatchException e){
								System.out.println("Masukkan Bilangan Bulat Positif!!!");
								input.nextLine();
								repeat=1;
							}    
						}while(repeat==1);
						break;
					case 3:
						repeat=1;
						do{
							try{
								SubMenuVolumeAndSurfaceArea();
								int choice3 = input.nextInt();
								switch(choice3){
									case 1:
											VolumeofPrism();
											repeat=0;
											break;
									case 2:
											SurfaceAreaofPrism();
											repeat=0;
											break;
									default:
											System.out.println("Pilihan Salah");
											break;
								}
							}catch(InputMismatchException e){
								System.out.println("Masukkan Bilangan Bulat Positif!!!");
								input.nextLine();
								repeat=1;
							}    
						}while(repeat==1);
						break;
					case 4:
						repeat=1;
						do{
							try{
								SubMenuVolumeAndSurfaceArea();
								int choice4 = input.nextInt();
								switch(choice4){
									case 1:
											VolumeofTetrahedron();
											repeat=0;
											break;
									case 2:
											SurfaceAreaofTetrahedron();
											repeat=0;
											break;
									default:
											System.out.println("Pilihan Salah");
											break;
								}
							}catch(InputMismatchException e){
								System.out.println("Masukkan Bilangan Bulat Positif!!!");
								input.nextLine();
								repeat=1;
							}    
						}while(repeat==1);
						break;
					case 5:
						repeat=1;
						do{
							try{    
								SubMenuVolumeAndSurfaceArea();
								int choice5 = input.nextInt();
								switch(choice5){
									case 1:
											VolumeofPyramid();
											repeat=0;
											break;
									case 2:
											SurfaceAreaofPyramid();
											repeat=0;
											break;
									default:
											System.out.println("Pilihan Salah");
											break;
								}
							}catch(InputMismatchException e){
								System.out.println("Masukkan Bilangan Bulat Positif!!!");
								input.nextLine();
								repeat=1;
							}    
						}while(repeat==1);
						break;
					case 6:
						repeat=1;
						do{
							try{
								SubMenuVolumeAndSurfaceArea();
								int choice6 = input.nextInt();
								switch(choice6){
									case 1:
											VolumeofCylinder();
											repeat=0;
											break;
									case 2:
											SurfaceAreaofCylinder();
											repeat=0;
											break;
									default:
											System.out.println("Pilihan Salah");
											break;
								}
							}catch(InputMismatchException e){
								System.out.println("Masukkan Bilangan Bulat Positif!!!");
								input.nextLine();
								repeat=1;
							}    
						}while(repeat==1);
						break;
					case 7:
						repeat=1;
						do{
							try{
								SubMenuVolumeAndSurfaceArea();
								int choice7 = input.nextInt();
								switch(choice7){
									case 1:
											VolumeofCone();
											repeat=0;
											break;
									case 2:
											SurfaceAreaofCone();
											repeat=0;
											break;
									default:
											System.out.println("Pilihan Salah");
											break;
								}
							}catch(InputMismatchException e){
								System.out.println("Masukkan Bilangan Bulat Positif!!!");
								input.nextLine();
								repeat=1;
							}    
						}while(repeat==1);
						break;
					case 8:
						repeat=1;
						do{
							try{
								SubMenuVolumeAndSurfaceArea();
								int choice8 = input.nextInt();
								switch(choice8){
									case 1:
											VolumeofSphere();
											repeat=0;
											break;
									case 2:
											SurfaceAreaofSphere();
											repeat=0;
											break;
									default:
											System.out.println("Pilihan Salah");
											break;
								}
							}catch(InputMismatchException e){
								System.out.println("Masukkan Bilangan Bulat Positif!!!");
								input.nextLine();
								repeat=1;
							}    
						}while(repeat==1);
						break;
					default:
							System.out.println("Pilihan Salah");
							break;
				}
			}catch(InputMismatchException e){
				System.out.println("Masukkan Bilangan Bulat Positif!!!");
				input.nextLine();
				repeat=1;
			}
		
		}while(repeat==1);
	}
	
	public static void SubMenuAreaAndPerimeter(){
		System.out.println("Anda Ingin Menghitung Apa?");
		System.out.println("1. Luas");
		System.out.println("2. Keliling");
	}
	
	public static void SubMenuVolumeAndSurfaceArea(){
		System.out.println("Anda Ingin Menghitung Apa?");
		System.out.println("1. Volume");
		System.out.println("2. Luas Permukaan");
	}
	
	public static void AreaofSquare(){
		Scanner input = new Scanner(System.in);
		int repeat=0;
		do{
			try{
				System.out.println("Masukkan Panjang Sisi (dalam cm) (Contoh:50,34)");
				float length = input.nextFloat();
				System.out.println("Luas Persegi = "+ Math.pow(length,2));
				repeat=0;
			}catch(InputMismatchException e){
				System.out.println("Masukkan Angka!!!");
				input.nextLine();
				repeat=1;
			}catch(ArithmeticException e){
				System.out.println("Inputan Salah!!!");
				input.nextLine();
				repeat=1;
			}
		}while(repeat==1);
	}
	
	public static void PerimeterofSquare(){
		Scanner input = new Scanner(System.in);
		int repeat=0;
		do{
			try{
				System.out.println("Masukkan Panjang Sisi (dalam cm) (Contoh:50,34)");
				float length = input.nextFloat();
				System.out.println("Keliling Persegi = "+((double) 4*length));
				repeat=0;
			}catch(InputMismatchException e){
				System.out.println("Masukkan Angka!!!");
				input.nextLine();
				repeat=1;
			}catch(ArithmeticException e){
				System.out.println("Inputan Salah!!!");
				input.nextLine();
				repeat=1;
			}
		}while(repeat==1);
	}
	
	public static void AreaofRectangle(){
		Scanner input = new Scanner(System.in);
		int repeat=0;
		do{
			try{
				System.out.println("Masukkan Panjang Persegi Panjang (dalam cm) (Contoh:50,34)");
				float length = input.nextFloat();
				System.out.println("Masukkan Lebar Persegi Panjang (dalam cm) (Contoh: 34,5)");
				float width = input.nextFloat();
				System.out.println("Luas Persegi Panjang = "+((double) length*width));
				repeat=0;
			}catch(InputMismatchException e){
				System.out.println("Masukkan Angka!!!");
				input.nextLine();
				repeat=1;
			}catch(ArithmeticException e){
				System.out.println("Inputan Salah!!!");
				input.nextLine();
				repeat=1;
			}
		}while(repeat==1);
	}
	
	public static void PerimeterofRectangle(){
		Scanner input = new Scanner(System.in);
		int repeat=0;
		do{
			try{
				System.out.println("Masukkan Panjang Persegi Panjang (dalam cm) (Contoh:50,34)");
				float length = input.nextFloat();
				System.out.println("Masukkan Lebar Persegi Panjang (dalam cm) (Contoh: 34,5)");
				float width = input.nextFloat();
				System.out.println("Keliling Persegi Panjang = "+((double) 2*(length+width)));
				repeat=0;
			}catch(InputMismatchException e){
				System.out.println("Masukkan Angka!!!");
				input.nextLine();
				repeat=1;
			}catch(ArithmeticException e){
				System.out.println("Inputan Salah!!!");
				input.nextLine();
				repeat=1;
			}
		}while(repeat==1);
	}
	
	public static void AreaofTriangle(){
		Scanner input = new Scanner(System.in);
		int repeat=0;
		do{
			try{
				System.out.println("Masukkan Tinggi Segitiga (dalam cm) (Contoh:50,34)");
				float high = input.nextFloat();
				System.out.println("Masukkan Panjang Alas Segitiga (dalam cm) (Contoh: 34,5)");
				float length = input.nextFloat();
				System.out.println("Luas Segitiga = "+((double) 0.5*length*high));
				repeat=0;
			}catch(InputMismatchException e){
				System.out.println("Masukkan Angka!!!");
				input.nextLine();
				repeat=1;
			}catch(ArithmeticException e){
				System.out.println("Inputan Salah!!!");
				input.nextLine();
				repeat=1;
			}
		}while(repeat==1);
	}
	
	public static void PerimeterofTriangle(){
		Scanner input = new Scanner(System.in);
		int repeat=0;
		do{
			try{
				System.out.println("Masukkan Panjang Sisi Segitiga yang sama panjang (dalam cm) (Contoh:50,34)");
				float length = input.nextFloat();
				System.out.println("Masukkan Panjang Alas Segitiga (dalam cm) (Contoh: 34,5)");
				float width = input.nextFloat();
				System.out.println("Keliling Segitiga = "+((double) 2*length+width));
				repeat=0;
			}catch(InputMismatchException e){
				System.out.println("Masukkan Angka!!!");
				input.nextLine();
				repeat=1;
			}catch(ArithmeticException e){
				System.out.println("Inputan Salah!!!");
				input.nextLine();
				repeat=1;
			}
		}while(repeat==1);
	}
	
	public static void AreaofEquilateralTriangle(){
		Scanner input = new Scanner(System.in);
		int repeat=0;
		do{
			try{
				System.out.println("Masukkan Tinggi Segitiga (dalam cm) (Contoh:50,34)");
				float high = input.nextFloat();
				System.out.println("Masukkan Panjang Alas Segitiga (dalam cm) (Contoh: 34,5)");
				float length = input.nextFloat();
				System.out.println("Luas Segitiga = "+((double) 0.5*length*high));
				repeat=0;
			}catch(InputMismatchException e){
				System.out.println("Masukkan Angka!!!");
				input.nextLine();
				repeat=1;
			}catch(ArithmeticException e){
				System.out.println("Inputan Salah!!!");
				input.nextLine();
				repeat=1;
			}
		}while(repeat==1);
	}
	
	public static void PerimeterofEquilateralTriangle(){
		Scanner input = new Scanner(System.in);
		int repeat=0;
		do{
			try{
				System.out.println("Masukkan Panjang Sisi Segitiga yang sama panjang (dalam cm) (Contoh:50,34)");
				float length = input.nextFloat();
				System.out.println("Keliling Segitiga = "+((double) 3*length));
				repeat=0;
			}catch(InputMismatchException e){
				System.out.println("Masukkan Angka!!!");
				input.nextLine();
				repeat=1;
			}catch(ArithmeticException e){
				System.out.println("Inputan Salah!!!");
				input.nextLine();
				repeat=1;
			}
		}while(repeat==1);
	}
	
	public static void AreaofTrapezoid(){
		Scanner input = new Scanner(System.in);
		int repeat=0;
		do{
			try{
				System.out.println("Masukkan Tinggi Trapesium (dalam cm) (Contoh:50,34)");
				float high = input.nextFloat();
				System.out.println("Masukkan Panjang Sisi Atas Trapesium (dalam cm) (Contoh: 34,5)");
				float length_a = input.nextFloat();
				System.out.println("Masukkan Panjang Sisi Bawah Trapesium (dalam cm) (Contoh: 34,5)");
				float length_b = input.nextFloat();
				System.out.println("Luas Trapesium = "+((double) 0.5*(length_a+length_b)*high));
				repeat=0;
			}catch(InputMismatchException e){
				System.out.println("Masukkan Angka!!!");
				input.nextLine();
				repeat=1;
			}catch(ArithmeticException e){
				System.out.println("Inputan Salah!!!");
				input.nextLine();
				repeat=1;
			}
		}while(repeat==1);
	}
	
	public static void PerimeterofTrapezoid(){
		Scanner input = new Scanner(System.in);
		int repeat=0;
		do{
			try{
				System.out.println("Masukkan Panjang Sisi-sisinya (dalam cm) (Contoh:50,34)");
				System.out.println("Sisi 1");
				float length_a = input.nextFloat();
				System.out.println("Sisi 2");
				float length_b = input.nextFloat();
				System.out.println("Sisi 3");
				float length_c = input.nextFloat();
				System.out.println("Sisi 4");
				float length_d = input.nextFloat();
				System.out.println("Keliling Trapesium = "+((double) length_a+length_b+length_c+length_d));
				repeat=0;
			}catch(InputMismatchException e){
				System.out.println("Masukkan Angka!!!");
				input.nextLine();
				repeat=1;
			}catch(ArithmeticException e){
				System.out.println("Inputan Salah!!!");
				input.nextLine();
				repeat=1;
			}
		}while(repeat==1);
	}
	
	public static void AreaofParallelogram(){
		Scanner input = new Scanner(System.in);
		int repeat=0;
		do{
			try{
				System.out.println("Masukkan Tinggi Jajaran Genjang (dalam cm) (Contoh:50,34)");
				float high = input.nextFloat();
				System.out.println("Masukkan Panjang Alas Jajaran Genjang (dalam cm) (Contoh: 34,5)");
				float length = input.nextFloat();
				System.out.println("Luas Jajaran Genjang = "+((double) 0.5*length*high));
				repeat=0;
			}catch(InputMismatchException e){
				System.out.println("Masukkan Angka!!!");
				input.nextLine();
				repeat=1;
			}catch(ArithmeticException e){
				System.out.println("Inputan Salah!!!");
				input.nextLine();
				repeat=1;
			}
		}while(repeat==1);
	}
	
	public static void PerimeterofParallelogram(){
		Scanner input = new Scanner(System.in);
		int repeat=0;
		do{
			try{
				System.out.println("Masukkan Panjang Alas Jajaran Genjang (dalam cm) (Contoh:50,34)");
				float length = input.nextFloat();
				System.out.println("Masukkan Panjang Sisi Miring (dalam cm) (Contoh: 34,5)");
				float width = input.nextFloat();
				System.out.println("Keliling Jajaran Genjang = "+((double) 2*(length+width)));
				repeat=0;
			}catch(InputMismatchException e){
				System.out.println("Masukkan Angka!!!");
				input.nextLine();
				repeat=1;
			}catch(ArithmeticException e){
				System.out.println("Inputan Salah!!!");
				input.nextLine();
				repeat=1;
			}
		}while(repeat==1);
	}
	
	public static void AreaofCircle(){
		Scanner input = new Scanner(System.in);
		int repeat=0;
		do{
			try{
				System.out.println("Masukkan Jari-jari Lingkaran (dalam cm) (Contoh:50,34)");
				float radius = input.nextFloat();
				System.out.println("Luas Lingkaran = "+((double) 3.14*radius*radius));
				repeat=0;
			}catch(InputMismatchException e){
				System.out.println("Masukkan Angka!!!");
				input.nextLine();
				repeat=1;
			}catch(ArithmeticException e){
				System.out.println("Inputan Salah!!!");
				input.nextLine();
				repeat=1;
			}
		}while(repeat==1);
	}
	
	public static void PerimeterofCircle(){
		Scanner input = new Scanner(System.in);
		int repeat=0;
		do{
			try{
				System.out.println("Masukkan Jari-jari Lingkaran (dalam cm) (Contoh:50,34)");
				float radius = input.nextFloat();
				System.out.println("Keliling Segitiga = "+((double) 2*3.14*radius));
				repeat=0;
			}catch(InputMismatchException e){
				System.out.println("Masukkan Angka!!!");
				input.nextLine();
				repeat=1;
			}catch(ArithmeticException e){
				System.out.println("Inputan Salah!!!");
				input.nextLine();
				repeat=1;
			}
		}while(repeat==1);
	}
	
	public static void VolumeofCube(){
		Scanner input = new Scanner(System.in);
		int repeat=0;
		do{
			try{
				System.out.println("Masukkan Panjang sisi kubus (dalam cm) (Contoh:50,34)");
				float side = input.nextFloat();
				System.out.println("Volume Kubus = "+((double) side*side*side));
				repeat=0;
			}catch(InputMismatchException e){
				System.out.println("Masukkan Angka!!!");
				input.nextLine();
				repeat=1;
			}catch(ArithmeticException e){
				System.out.println("Inputan Salah!!!");
				input.nextLine();
				repeat=1;
			}
		}while(repeat==1);
	}
	
	public static void SurfaceAreaofCube(){
		Scanner input = new Scanner(System.in);
		int repeat=0;
		do{
			try{
				System.out.println("Masukkan Panjang sisi kubus (dalam cm) (Contoh:50,34)");
				float side = input.nextFloat();
				System.out.println("Luas Permukaan Kubus = "+((double) 6*side*side));
				repeat=0;
			}catch(InputMismatchException e){
				System.out.println("Masukkan Angka!!!");
				input.nextLine();
				repeat=1;
			}catch(ArithmeticException e){
				System.out.println("Inputan Salah!!!");
				input.nextLine();
				repeat=1;
			}
		}while(repeat==1);
	}
	
	public static void VolumeofBeam(){
		Scanner input = new Scanner(System.in);
		int repeat=0;
		do{
			try{
				System.out.println("Masukkan Panjang Balok (dalam cm) (Contoh:50,34)");
				float length = input.nextFloat();
				System.out.println("Masukkan Lebar Balok (dalam cm) (Contoh:50,34)");
				float width = input.nextFloat();
				System.out.println("Masukkan Tinggi Balok (dalam cm) (Contoh:50,34)");
				float height = input.nextFloat();
				System.out.println("Volume Balok = "+((double) length*width*height));
				repeat=0;
			}catch(InputMismatchException e){
				System.out.println("Masukkan Angka!!!");
				input.nextLine();
				repeat=1;
			}catch(ArithmeticException e){
				System.out.println("Inputan Salah!!!");
				input.nextLine();
				repeat=1;
			}
		}while(repeat==1);
	}
	
	public static void SurfaceAreaofBeam(){
		Scanner input = new Scanner(System.in);
		int repeat=0;
		do{
			try{
				System.out.println("Masukkan Panjang Balok (dalam cm) (Contoh:50,34)");
				float length = input.nextFloat();
				System.out.println("Masukkan Lebar Balok (dalam cm) (Contoh:50,34)");
				float width = input.nextFloat();
				System.out.println("Masukkan Tinggi Balok (dalam cm) (Contoh:50,34)");
				float height = input.nextFloat();
				System.out.println("Luas Permukaan Balok = "+((double) 2*((length*width)+(width*height)+(length*height))));
				repeat=0;
			}catch(InputMismatchException e){
				System.out.println("Masukkan Angka!!!");
				input.nextLine();
				repeat=1;
			}catch(ArithmeticException e){
				System.out.println("Inputan Salah!!!");
				input.nextLine();
				repeat=1;
			}
		}while(repeat==1);
	}
	
	public static void VolumeofPrism(){
		Scanner input = new Scanner(System.in);
		int repeat=0;
		do{
			try{
				System.out.println("Masukkan Panjang Alas Segitiga (dalam cm) (Contoh:50,34)");
				float length = input.nextFloat();
				System.out.println("Masukkan Tinggi Segitiga (dalam cm) (Contoh:50,34)");
				float heighttriangle = input.nextFloat();
				System.out.println("Masukkan Tinggi Prisma (dalam cm) (Contoh:50,34)");
				float heightprism = input.nextFloat();
				System.out.println("Volume Prisma = "+((double) 0.5*length*heighttriangle*heightprism));
				repeat=0;
			}catch(InputMismatchException e){
				System.out.println("Masukkan Angka!!!");
				input.nextLine();
				repeat=1;
			}catch(ArithmeticException e){
				System.out.println("Inputan Salah!!!");
				input.nextLine();
				repeat=1;
			}
		}while(repeat==1);
	}
	
	public static void SurfaceAreaofPrism(){
		Scanner input = new Scanner(System.in);
		int repeat=0;
		do{
			try{
				System.out.println("Masukkan Panjang Alas Segitiga (dalam cm) (Contoh:50,34)");
				float length = input.nextFloat();
				System.out.println("Masukkan Tinggi Segitiga (dalam cm) (Contoh:50,34)");
				float heighttriangle = input.nextFloat();
				System.out.println("Masukkan Tinggi Prisma (dalam cm) (Contoh:50,34)");
				float heightprism = input.nextFloat();
				System.out.println("Luas Permukaan Prisma = "+((double) (2*(0.5*length*heighttriangle*heightprism)+3*(length*heightprism))));
				repeat=0;
			}catch(InputMismatchException e){
				System.out.println("Masukkan Angka!!!");
				input.nextLine();
				repeat=1;
			}catch(ArithmeticException e){
				System.out.println("Inputan Salah!!!");
				input.nextLine();
				repeat=1;
			}
		}while(repeat==1);
	}
	
	public static void VolumeofTetrahedron(){
		Scanner input = new Scanner(System.in);
		int repeat=0;
		do{
			try{
				System.out.println("Masukkan Panjang sisi Limas (dalam cm) (Contoh:50,34)");
				float side = input.nextFloat();
				System.out.println("Volume Limas Segitiga = "+((double) 1/12*Math.sqrt(2*Math.pow(side,3))));
				repeat=0;
			}catch(InputMismatchException e){
				System.out.println("Masukkan Angka!!!");
				input.nextLine();
				repeat=1;
			}catch(ArithmeticException e){
				System.out.println("Inputan Salah!!!");
				input.nextLine();
				repeat=1;
			}
		}while(repeat==1);
	}
	
	public static void SurfaceAreaofTetrahedron(){
		Scanner input = new Scanner(System.in);
		int repeat=0;
		do{
			try{
				System.out.println("Masukkan Panjang sisi Limas (dalam cm) (Contoh:50,34)");
				float side = input.nextFloat();
				System.out.println("Luas Permukaan Limas Segitiga = "+((double) Math.sqrt(3*Math.pow(side,2))));
				repeat=0;
			}catch(InputMismatchException e){
				System.out.println("Masukkan Angka!!!");
				input.nextLine();
				repeat=1;
			}catch(ArithmeticException e){
				System.out.println("Inputan Salah!!!");
				input.nextLine();
				repeat=1;
			}
		}while(repeat==1);
	}
	
	public static void VolumeofPyramid(){
		Scanner input = new Scanner(System.in);
		int repeat=0;
		do{
			try{
				System.out.println("Masukkan Panjang sisi Persegi (dalam cm) (Contoh:50,34)");
				float side = input.nextFloat();
				System.out.println("Masukkan Tinggi Limas (dalam cm) (Contoh:50,34)");
				float height = input.nextFloat();
				System.out.println("Volume Limas Segiempat = "+((double) 1/3*Math.pow(side,2)*height));
				repeat=0;
			}catch(InputMismatchException e){
				System.out.println("Masukkan Angka!!!");
				input.nextLine();
				repeat=1;
			}catch(ArithmeticException e){
				System.out.println("Inputan Salah!!!");
				input.nextLine();
				repeat=1;
			}
		}while(repeat==1);
	}
	
	public static void SurfaceAreaofPyramid(){
		Scanner input = new Scanner(System.in);
		int repeat=0;
		do{
			try{
				System.out.println("Masukkan Panjang sisi Persegi (dalam cm) (Contoh:50,34)");
				float side = input.nextFloat();
				System.out.println("Masukkan Tinggi Limas (dalam cm) (Contoh:50,34)");
				float height = input.nextFloat();
				System.out.println("Luas Permukaan Limas Segitiga = "+((double) side*(side+Math.sqrt(Math.pow(side,2)+4*Math.pow(height,2)))));
				repeat=0;
			}catch(InputMismatchException e){
				System.out.println("Masukkan Angka!!!");
				input.nextLine();
				repeat=1;
			}catch(ArithmeticException e){
				System.out.println("Inputan Salah!!!");
				input.nextLine();
				repeat=1;
			}
		}while(repeat==1);
	}
	
	public static void VolumeofCylinder(){
		Scanner input = new Scanner(System.in);
		int repeat=0;
		do{
			try{
				System.out.println("Masukkan Jari-jari (dalam cm) (Contoh:50,34)");
				float radius = input.nextFloat();
				System.out.println("Masukkan Tinggi Tabung (dalam cm) (Contoh:50,34)");
				float height = input.nextFloat();
				System.out.println("Volume Tabung = "+((double) 3.14*Math.pow(radius,2)*height));
				repeat=0;
			}catch(InputMismatchException e){
				System.out.println("Masukkan Angka!!!");
				input.nextLine();
				repeat=1;
			}catch(ArithmeticException e){
				System.out.println("Inputan Salah!!!");
				input.nextLine();
				repeat=1;
			}
		}while(repeat==1);
	}
	
	public static void SurfaceAreaofCylinder(){
		Scanner input = new Scanner(System.in);
		int repeat=0;
		do{
			try{
				System.out.println("Masukkan Jari-jari (dalam cm) (Contoh:50,34)");
				float radius = input.nextFloat();
				System.out.println("Masukkan Tinggi Tabung (dalam cm) (Contoh:50,34)");
				float height = input.nextFloat();
				System.out.println("Luas Permukaan Tabung = "+((double) 2*3.14*radius*(height+radius)));
				repeat=0;
			}catch(InputMismatchException e){
				System.out.println("Masukkan Angka!!!");
				input.nextLine();
				repeat=1;
			}catch(ArithmeticException e){
				System.out.println("Inputan Salah!!!");
				input.nextLine();
				repeat=1;
			}
		}while(repeat==1);
	}
	
	public static void VolumeofCone(){
		Scanner input = new Scanner(System.in);
		int repeat=0;
		do{
			try{
				System.out.println("Masukkan Jari-jari (dalam cm) (Contoh:50,34)");
				float radius = input.nextFloat();
				System.out.println("Masukkan Tinggi Kerucut (dalam cm) (Contoh:50,34)");
				float height = input.nextFloat();
				System.out.println("Volume Kerucut = "+((double) 1/3*3.14*Math.pow(radius,2)*height));
				repeat=0;
			}catch(InputMismatchException e){
				System.out.println("Masukkan Angka!!!");
				input.nextLine();
				repeat=1;
			}catch(ArithmeticException e){
				System.out.println("Inputan Salah!!!");
				input.nextLine();
				repeat=1;
			}
		}while(repeat==1);
	}
	
	public static void SurfaceAreaofCone(){
		Scanner input = new Scanner(System.in);
		int repeat=0;
		do{
			try{
				System.out.println("Masukkan Jari-jari (dalam cm) (Contoh:50,34)");
				float radius = input.nextFloat();
				System.out.println("Masukkan Tinggi Kerucut (dalam cm) (Contoh:50,34)");
				float height = input.nextFloat();
				System.out.println("Luas Permukaan Kerucut = "+((double) 3.14*radius*(Math.sqrt(Math.pow(height,2)+Math.pow(radius,2))+radius)));
				repeat=0;
			}catch(InputMismatchException e){
				System.out.println("Masukkan Angka!!!");
				input.nextLine();
				repeat=1;
			}catch(ArithmeticException e){
				System.out.println("Inputan Salah!!!");
				input.nextLine();
				repeat=1;
			}
		}while(repeat==1);
	}
	
	public static void VolumeofSphere(){
		Scanner input = new Scanner(System.in);
		int repeat=0;
		do{
			try{
				System.out.println("Masukkan Jari-jari (dalam cm) (Contoh:50,34)");
				float radius = input.nextFloat();
				System.out.println("Volume Bola = "+((double) 4/3*3.14*Math.pow(radius,3)));
				repeat=0;
			}catch(InputMismatchException e){
				System.out.println("Masukkan Angka!!!");
				input.nextLine();
				repeat=1;
			}catch(ArithmeticException e){
				System.out.println("Inputan Salah!!!");
				input.nextLine();
				repeat=1;
			}
		}while(repeat==1);
	}
	
	public static void SurfaceAreaofSphere(){
		Scanner input = new Scanner(System.in);
		int repeat=0;
		do{
			try{
				System.out.println("Masukkan Jari-jari (dalam cm) (Contoh:50,34)");
				float radius = input.nextFloat();
				System.out.println("Luas Permukaan Bola = "+((double) 4*3.14*Math.pow(radius,2)));
				repeat=0;
			}catch(InputMismatchException e){
				System.out.println("Masukkan Angka!!!");
				input.nextLine();
				repeat=1;
			}catch(ArithmeticException e){
				System.out.println("Inputan Salah!!!");
				input.nextLine();
				repeat=1;
			}
		}while(repeat==1);
	}
	
	public static void main(String[] args){
		Scanner input = new Scanner(System.in);
		int repeat=1;
		System.out.println("Program Menghitung Luas dan Keliling Bangun Datar serta Volume dan Luas Permukaan Bangun Ruang");
		do{
			MainMenu();
			try{
				int choise = input.nextInt();
				switch(choise){
					case 1:
						SubMenuTwoDimensionalGeometry();
						repeat=1;
						break;
					case 2:
						SubMenuThreeDimensionalGeometry();
						repeat=1;
						break;
					case 3:
						repeat = 0;
						break;
					default:
						System.out.println("Pilihan Salah");
						repeat = 1;
						break;
				}
			}catch(InputMismatchException e){
				System.out.println("Masukkan Bilangan Bulat Positif!!!");
				input.nextLine();
				repeat=1;
				continue;
			}
		}while(repeat==1);
		
	}
}