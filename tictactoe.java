import java.util.*;
public class tictactoe {
    static byte[][] table = new byte[3][3];
    static boolean[][] emptytable = new boolean[3][3];
    static int choice;
    static boolean playagain=false;
    static boolean exit = false;
    
    public static void mainmenu(){
        Scanner input = new Scanner(System.in);
        System.out.println("Menu Utama");
        System.out.println("1. Play Game");
        System.out.println("2. Peraturan");
        System.out.println("3. Keluar");
        System.out.print("==>");
        try{
            choice = input.nextInt();
        }catch(InputMismatchException e){
            
        }
    }
    
    public static void ruleofgame(){
        System.out.println("Peraturan");
        System.out.println("-> Pemain Pertama Menggunakan Simbol X");
        System.out.println("-> Pemain Kedua Menggunakan Simbol O");
        System.out.println("-> Pemenang adalah Pemain yang pertama kali membuat pola lurus");
    }
    
    public static void playgame(){
        boolean finish = false ;
        do{
            resetGame();
            do{
                printBoard();
                playerInput ((byte)1);
                if ( checkWinner (table)==1){
                    printBoard();
                    System.out.println("Selesai");
                    System.out.println("Pemain 1 Menang");
                    break ;
                }else if (checkDraw ()){
                    printBoard();
                    System.out.println("Imbang");
                    finish = true ;
                    break ;	
                }

                printBoard();
                playerInput ((byte)2);

                if ( checkWinner (table)==2){
                    printBoard();
                    System.out.println("Selesai");
                    System.out.println("Pemain 2 Menang");
                    break ;
                }else if (checkDraw ()){
                    printBoard();
                    System.out.println("Imbang");
                    finish = true ;
                    break ;	
                }
                finish = checkWinner  (table)==0? false: true ;
            } while (!finish);
            AsktoPlayAgain();
        } while (playagain);
    }
    
    public static void playerInput (byte player){
        int tryInput = 0;
        byte row =0, column = 0;
        do {
            System.out.println("==> Giliran Pemain "+player);
            try{
                System.out.println("Dimana Anda ingin Turun?");
                Scanner inputData = new Scanner (System.in);
                System.out.print("Baris ke ");
                row = inputData.nextByte();
                System.out.print("Kolom ke ");
                column = inputData.nextByte();

                if (emptytable [row-1][column-1]){
                    emptytable [row-1][column-1] = false;
                    table [row-1][column-1] = player ;
                    tryInput = 0;
                }else {
                    System.out.println("Posisi telah terisi. Coba posisi lain!!");
                    tryInput = 1;
                }
            }

            catch (ArrayIndexOutOfBoundsException ex){
                System.out.println("Baris atau Kolom yang Anda Masukkan Salah!!!");
                tryInput = 1;
            }

            catch (InputMismatchException e){
                System.out.println("Silahkan Masukkan Angka!!!");
            }
	}while (tryInput==1 || ((row<1 || row > 3) || (column <1 || column >3 )));
    }
    
    public static void printBoard(){
        System.out.println("Papan Permainan");
        for ( int row = 0; row <3 ; row ++ ){
            for ( int space = 0; space<3 ; space++ ){
                System.out.print(" ");
                if(space == 3){
                System.out.print("|");
                }
            }
            for ( int column = 0; column<3 ; column ++ ){
                if (table [row][column] == 1 ){
                        System.out.print("X");
                }else if (table [row][column] == 2 ){
                        System.out.print("O");
                }else {
                        System.out.print(" ");
                }
                System.out.print("|");
            }
            System.out.println();
        }
        System.out.println();
    }
	
	
    public static int checkWinner (byte [][] table){
        int winner = 0;

        if((((table [0][0]==1)&&(table [0][1]==1)&&(table [0][2]==1)) ||
            ((table [1][0]==1)&&(table [1][1]==1)&&(table [1][2]==1)) ||
            ((table [2][0]==1)&&(table [2][1]==1)&&(table [2][2]==1)) ||
            ((table [0][0]==1)&&(table [1][0]==1)&&(table [2][0]==1)) ||
            ((table [0][1]==1)&&(table [1][1]==1)&&(table [2][1]==1)) ||
            ((table [0][2]==1)&&(table [1][2]==1)&&(table [2][2]==1)) ||
            ((table [0][0]==1)&&(table [1][1]==1)&&(table [2][2]==1)) ||
            ((table [0][2]==1)&&(table [1][1]==1)&&(table [2][0]==1)) )){

            winner = 1 ;

        }else if((((table [0][0]==2)&&(table [0][1]==2)&&(table [0][2]==2)) ||
            ((table [1][0]==2)&&(table [1][1]==2)&&(table [1][2]==2)) ||
            ((table [2][0]==2)&&(table [2][1]==2)&&(table [2][2]==2)) ||
            ((table [0][0]==2)&&(table [1][0]==2)&&(table [2][0]==2)) ||
            ((table [0][1]==2)&&(table [1][1]==2)&&(table [2][1]==2)) ||
            ((table [0][2]==2)&&(table [1][2]==2)&&(table [2][2]==2)) ||
            ((table [0][0]==2)&&(table [1][1]==2)&&(table [2][2]==2)) ||
            ((table [0][2]==2)&&(table [1][1]==2)&&(table [2][0]==2)) )){
            winner = 2 ;
        }
        return winner;
    }
	
    public static void resetGame (){
        for ( int row = 0; row<3 ; row++ ){
            for ( int column = 0; column<3 ; column ++ ){
                table [row][column]=0;
                emptytable [row][column]= true ;
            }
        }

    }
		
		
    public static boolean checkDraw (){
        boolean result = true ;
        for ( byte row = 0; row<3 ; row++ ){
            for ( byte column = 0; column<3 ; column++ ){
                if (emptytable [row][column]){
                    result = false ;
                    return result ;
                }
            }
        }
        return result ;
    }
    
    public static boolean AsktoPlayAgain() {
        System.out.println("Bermain Lagi?");
        boolean askagain = false ;
        do {
            System.out.print(" Tekan \"O\" untuk Memulai Game, atau \"N\" untuk Keluar===>  ");
            Scanner userinput = new Scanner (System.in);
            String input = userinput.next();
            if (input.equalsIgnoreCase("o")){
                playagain = true ;
                askagain = false ;

            }else if (input.equalsIgnoreCase("n")){
                playagain = false ;
                askagain = false ;
            }else{
                System.out.print(" Inputan Salah");
                askagain = true ;
            }
        }while (askagain);
        return playagain;
    }
		
		
    public static void BacktoMainMenu() {
        Scanner inputChoice = new Scanner (System.in);
        String input ;

        do {
            System.out.println("Tekan \"B\" Untuk kembali ke Main Menu, atau \"E\" untuk Keluar");
            System.out.print("==> ");
            input =inputChoice.next();
            if (input.equalsIgnoreCase("b")){
                exit = false ;
                break ;
            }else if (input.equalsIgnoreCase("e")){
                exit = false ;
                break ;
            }else{
                System.out.print("Pilih Antara \"B\" atau \"E\"");
            }
        }while (!input.equalsIgnoreCase("b") && !input.equalsIgnoreCase("e"));
    }

    public static void main(String[] args) {
        do {
            mainmenu();
            switch (choice){
            case 1 :
                playgame ();
                BacktoMainMenu ();
                break ;
            case 2 :
                ruleofgame();
                BacktoMainMenu ();
                break ;
            case 3 :
                exit = true ;
                break ;
            default :
                System.out.println("Pilihan Salah");
                exit = false ;
            }
        }while (!exit);
    }    
}